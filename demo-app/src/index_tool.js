import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// import { ColorTool } from './components/ColorTool';
// import { CarTool } from './components/CarTool';

import { ColorTool, CarTool } from './components';

const colorList = ['red', 'blue', 'green', 'black', 'purple'];
const carList = [
  { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2017, color: 'green', price: 32000 },
  { id: 2, make: 'Tesla', model: 'S', year: 2018, color: 'red', price: 120000 },
];

const CarToolWithList = (props) => {
  return <CarTool cars={carList} {...props} />;
}

ReactDOM.render(
  <Router>
    <Route path="/car-tool" component={CarToolWithList} />
  </Router>,
  document.querySelector('#root'),
);

