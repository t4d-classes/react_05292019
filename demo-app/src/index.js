import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, bindActionCreators, combineReducers } from 'redux';
import { connect, Provider } from 'react-redux';

import { useForm } from './hooks/form';

const ADD_ACTION = '[CalcTool] Add';
const SUBTRACT_ACTION = '[CalcTool] Subtract';


const resultReducer = (state = 0, action) => {

  switch (action.type) {
    case ADD_ACTION:
      return state + action.payload;
    case SUBTRACT_ACTION:
      return state - action.payload;
    default:
      return state;
  }

};

const historyReducer = (state = [], action) => {

  switch (action.type) {
    case ADD_ACTION:
    case SUBTRACT_ACTION:
      return state.concat(action.type + ' ' + action.payload);
    default:
      return state;
  }

};

const calcReducer = (state = {}, action) => {
  return {
    result: resultReducer(state.result, action),
    history: historyReducer(state.history, action),
  };
};



// const createStore = (reducerFn) => {

//   let currentState = undefined;
//   const subscribers = [];

//   return {
//     getState: () => currentState,
//     dispatch: (action) => {
//       currentState = reducerFn(currentState, action);
//       subscribers.forEach(cb => cb());
//     },
//     subscribe: (cb) => {
//       subscribers.push(cb);
//       return () => {
//         subscribers.splice(subscribers.indexOf(cb), 1);
//       };
//     },
//   };

// };

const calcStore = createStore(combineReducers({
  result: resultReducer,
  history: historyReducer,
}));

const createAddAction = payload => ({ type: ADD_ACTION, payload });
const createSubtractAction = payload => ({ type: SUBTRACT_ACTION, payload });

// actions.add = value => dispatch(createAddAction(value))

// const bindActionCreators = (actionCreators, dispatch) => {
//   return Object.keys(actionCreators).reduce( (boundActions, actionKey) => {
//     boundActions[actionKey] = (...params) => dispatch(actionCreators[actionKey](...params));
//     return boundActions;
//   }, {} );
// };

export const CalcTool = ({ result, history, onAdd, onSubtract  }) => {

  const [ calcForm, change ] = useForm({
    numInput: 0,
  });

  return <>
    <form>
      Result: {result}<br />
      Input: <input type="number" name="numInput" value={calcForm.numInput} onChange={change} /><br />
      <div>
        <button type="button" onClick={() => onAdd(calcForm.numInput)}>Add</button>
        <button type="button" onClick={() => onSubtract(calcForm.numInput)}>Subtract</button>
      </div>
    </form>
    <ul>
      {history.map(entry => <li>{entry}</li>)}
    </ul>
  </>;

}

// const { Provider, Consumer } = React.createContext(null);

// const connect = (mapStateToPropsFn, mapDispatchToPropsFn) => {

//   return PresentationalComponent => {

//     class ContainerComponent extends React.Component {

//       constructor(props) {
//         super(props);

//         this.dispatchFns = mapDispatchToPropsFn(props.store.dispatch);
//       }

//       componentDidMount() {
//         this.unsubscribeStore = this.props.store.subscribe(() => {
//           this.forceUpdate();
//         });
//       }

//       componentWillUnmount() {
//         this.unsubscribeStore();
//       }

//       render() {

//         const stateProps = mapStateToPropsFn(this.props.store.getState());

//         return <PresentationalComponent {...this.dispatchFns} {...stateProps} />;
//       }

//     }

//     return () => <Consumer>{value => <ContainerComponent store={value} />}</Consumer>;
//   };

// };

const createCalcToolContainer = connect(
  (state) => ({ result: state.result, history: state.history }),
  (dispatch) => bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
  }, dispatch),
);

const CalcToolContainer = createCalcToolContainer(CalcTool);

ReactDOM.render(
  <Provider store={calcStore}>
    <CalcToolContainer />
  </Provider>,
  document.querySelector('#root'),
);

