import React from 'react';
import PropTypes from 'prop-types';

export const ToolHeader = ({ children }) => {

  return <header id="tool-header">
    {children} {/* containment pattern */}
  </header>;
};

ToolHeader.defaultProps = {
  children: <h1>Tool Header</h1>,
};

ToolHeader.propTypes = {
  headerText: PropTypes.element,
};
