import React from 'react';

import { ViewCarRow } from './ViewCarRow';
import { EditCarRow } from './EditCarRow';

export const CarTable = (props) => {

  const {
    cars, editCarId,
    onEditCar, onDeleteCar,
    onSaveCar, onCancelCar,
    editRowComponent: EditRowComponent = EditCarRow
  } = props;

  return <table>
    <thead>
      <tr>
        {/* {Object.keys(cars[0]).map( (field, index) =>
          <th key={index}>{field.slice(0,1).toUpperCase() + field.slice(1)}</th> )} */}
        <th>Id</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>Color</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {cars.map(car => editCarId === car.id
          ? <EditRowComponent car={car} key={car.id}
              onSaveCar={onSaveCar} onCancelCar={onCancelCar} />
          : <ViewCarRow car={car} key={car.id}
              onEditCar={onEditCar} onDeleteCar={onDeleteCar} />)}
    </tbody>
  </table>;

};
