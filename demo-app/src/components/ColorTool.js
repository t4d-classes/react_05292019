import React, { useState } from 'react';

import { ColorToolHeader } from './ColorToolHeader';
import { UnorderedList } from './UnorderedList';
import { ColorForm } from './';

export const ColorListItem = ({ item }) => <span>{item.toUpperCase()}</span>;

export const ColorTool = (props) => {

  const [ colors, setColors ] = useState(props.colors.concat());

  const addColor = (color) => {
    setColors(colors.concat(color));
  };

  return <>
    <ColorToolHeader version="2" />
    <UnorderedList items={colors}
      component={ColorListItem} />
    <ColorForm buttonText="Add Color" onSubmitColor={addColor} />
  </>;

};