import React from 'react';

import { useForm } from '../hooks/form';

export const AddColorForm = ({ onAddColor }) => {
  return <ColorForm buttonText="Add Color" color="" onSubmitColor={onAddColor} />;
}

export const EditColorForm = ({ onReplaceColor, color }) => {
  return <ColorForm buttonText="Update Color" color={color} onSubmitColor={onReplaceColor} />;
}


export const ColorForm = ({
  buttonText = 'Submit Color',
  onSubmitColor,
  color: initialColor
}) => {

  const [ colorForm, change, reset ] = useForm({
    color: initialColor,
  });

  const submitColor = () => {
    onSubmitColor(colorForm.color);
    reset();
  };

  return <form>
    <div>
      <label htmlFor="color-input">Color</label>
      <input type="text" id="color-input" name="color"
        value={colorForm.color} onChange={change} />
    </div>
    <button type="button" onClick={submitColor}>{buttonText}</button>
  </form>;

};