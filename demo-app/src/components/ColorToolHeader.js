import React from 'react';

import { ToolHeader } from './ToolHeader';

// specialization pattern
export const ColorToolHeader = ({ version = 1 }) => {

  return <ToolHeader>
    <h1>Color Tool</h1>
    <small>Version {version}</small>
  </ToolHeader>;
};