import React, { useState } from 'react';
import { Switch, Route, Link, withRouter } from 'react-router-dom';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { AddCarForm } from './AddCarForm';
import { EditCarFormRow } from './EditCarFormRow';

export const CarTool = ({ cars: initialCars, match, history, location }) => {

  console.log(match);
  console.log(history);

  const queryParams = new URLSearchParams(location.search);

  for (let queryParam of queryParams) {
    console.log(queryParam);
  }

  const [ editCarId, setEditCarId ] = useState(-1);
  const [ cars, setCars ] = useState(initialCars.concat());

  const editCar = carId => {
    setEditCarId(carId);
  };

  const cancelCar = () => {
    setEditCarId(-1);
  };

  const appendCar = (car) => {
    setCars(cars.concat({
      ...car,
      id: Math.max(...cars.map(c => c.id), 0) + 1,
    }));
    setEditCarId(-1);
    history.push(match.path + '/', { testProp: 3 });
  };

  const deleteCar = carId => {
    setCars(cars.filter(c => c.id !== carId));
    setEditCarId(-1);
  };

  const replaceCar = car => {
    const newCars = cars.concat();
    newCars[newCars.findIndex(c => c.id === car.id)] = car;
    setCars(newCars);
    setEditCarId(-1);
  };

  return <>
    <ToolHeader>
      <h1>Car Tool</h1>
      <nav>
        <ul>
          <li><Link to={match.path+ '/'}>Home</Link></li>
          <li><Link to={{
            pathname: match.path + '/addcar',
            state: { testProp: 2 },
          }}>Add Car</Link></li>
        </ul>
      </nav>
    </ToolHeader>
    <Switch>
      <Route path={match.path} exact render={() =>
        <CarTable cars={cars} editCarId={editCarId}
          onEditCar={editCar} onDeleteCar={deleteCar}
          onSaveCar={replaceCar} onCancelCar={cancelCar}
          editRowComponent={EditCarFormRow} />} />
      <Route path={match.path + '/addcar'} render={() => {

        const AddCarFormWithRouter = withRouter(AddCarForm);

        return <AddCarFormWithRouter onAddCar={appendCar} />;
      }} />
    </Switch>
  </>;

};
