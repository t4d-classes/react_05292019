import React from 'react';

import { useForm } from '../hooks/form';

export const EditCarRow = ({ car, onSaveCar, onCancelCar }) => {

  const [ carForm, change ] = useForm({ ...car });

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" name="make" value={carForm.make} onChange={change} /></td>
    <td><input type="text" name="model" value={carForm.model} onChange={change} /></td>
    <td><input type="number" name="year" value={carForm.year} onChange={change} /></td>
    <td><input type="text" name="color" value={carForm.color} onChange={change} /></td>
    <td><input type="number" name="price" value={carForm.price} onChange={change} /></td>
    <td>
      <button type="button" onClick={() => onSaveCar({ ...carForm })}>Save</button>
      <button type="button" onClick={onCancelCar}>Cancel</button>
    </td>
  </tr>;

};