import React from 'react';

export const UnorderedList = ({ items, component: Component }) => {

  return <ul>
    {items.map((item, index) => <li key={index}>
      <Component item={item} />
    </li>)}
  </ul>;

};