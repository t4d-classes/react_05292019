import React from 'react';

import { CarForm } from './CarForm';

export const AddCarForm = ({ onAddCar, location }) => {

  console.log(location);

  return <CarForm onSubmitCar={onAddCar} buttonText="Add Car" />;
};