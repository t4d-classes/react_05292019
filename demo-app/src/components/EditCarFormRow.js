import React from 'react';

import { EditCarForm } from './EditCarForm';

export const EditCarFormRow = ({ car, onSaveCar, onCancelCar }) => {

  return <tr>
    <td colSpan="6">
      <EditCarForm car={car} onSaveCar={onSaveCar} />
    </td>
    <td>
      <button type="button" onClick={onCancelCar}>Cancel</button>
    </td>
  </tr>;

};
