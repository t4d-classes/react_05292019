Exercise 9

https://gitlab.com/t4d-classes/react_05292019

1. Upgrade Car Table, Car View Row, and Car Edit Row to support the following configuration object.

config = [
  { field: 'make', caption: 'Make', order: 1 },
  { field: 'model', caption: 'Model', order: 2 },
  { field: 'year', caption: 'Production Year', order: 3 },
  { field: 'color', caption: 'Body Color', order: 5 },
  { field: 'price', caption: 'Cost', order: 3 },
]

field is the name of the property on car object
caption is the header for the column which displays the field
the order is the order of the columns

2. Ensure it works.