# Welcome to React Class!

## Instructor

Eric Greene

## Schedule

Class:

- Wednesday - Friday: 9am to 5pm PDT

Breaks:

- Lunch: 12pm to 1pm
- Afternoon Break #1: 2:10pm to 2:20pm
- Afternoon Break #2: 3:30pm to 3:40pm
- Afternoon Break #3: 4:45pm to 4:55pm

## Course Outline

### JavaScript content will be included as needed in the class

- Day 1 - Overview of React, JSX, Function Components, Props, Prop Types, State Hook, Events
- Day 2 - Composition, Containment & Specialization, Class-based components
- Day 3 - React Router, Additional React Topics, Unit Tests

## Links

### Instructor's Resources

- [Accelebrate](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)
